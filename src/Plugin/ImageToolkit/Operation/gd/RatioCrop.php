<?php

namespace Drupal\ratio_crop\Plugin\ImageToolkit\Operation\gd;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\Crop;

/**
 * Defines GD2 Crop operation.
 *
 * @ImageToolkitOperation(
 *   id = "gd_crop_ratio",
 *   toolkit = "gd",
 *   operation = "crop_ratio",
 *   label = @Translation("Ratio Crop"),
 *   description = @Translation("Crops an image to match given aspect ratio.")
 * )
 */
class RatioCrop extends Crop {

  /**
   * {@inheritdoc}
   */
  protected function arguments() {
    $arguments = parent::arguments();
    $arguments['width']['required'] = TRUE;
    $arguments['height']['required'] = TRUE;
    return $arguments;
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments) {
    $original_resource = $this->getToolkit()->getImage();
    $result = imagecrop($original_resource, $arguments);
    if ($result) {
      $this->getToolkit()->setImage($result);
      return TRUE;
    }
    return FALSE;
  }

}
