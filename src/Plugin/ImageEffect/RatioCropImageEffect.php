<?php

namespace Drupal\ratio_crop\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Crops an image resource to defined ratio, keeping the size largest possible.
 *
 * @ImageEffect(
 *   id = "image_crop_ratio",
 *   label = @Translation("Ratio crop"),
 *   description = @Translation("Crops an image to predefined size by cropping the larger dimension side.")
 * )
 */
class RatioCropImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['aspect_ratio'] = [
      '#title' => $this->t('Aspect Ratio'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $this->configuration['aspect_ratio'],
      '#attributes' => ['placeholder' => 'W:H'],
      '#description' => $this->t('Set an aspect ratio <b>eg: 16:9</b>'),
    ];
    $form['anchor'] = [
      '#type' => 'radios',
      '#title' => t('Anchor'),
      '#options' => [
        'left-top' => t('Top left'),
        'center-top' => t('Top center'),
        'right-top' => t('Top right'),
        'left-center' => t('Center left'),
        'center-center' => t('Center'),
        'right-center' => t('Center right'),
        'left-bottom' => t('Bottom left'),
        'center-bottom' => t('Bottom center'),
        'right-bottom' => t('Bottom right'),
      ],
      '#theme' => 'image_anchor',
      '#default_value' => $this->configuration['anchor'],
      '#description' => t('The part of the image that will be retained during the crop.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'anchor' => 'center-center',
      'aspect_ratio' => '1:1',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = [
      '#theme' => 'ratio_crop_summary',
      '#data' => $this->configuration,
    ];
    $summary += parent::getSummary();

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if (!preg_match('#^[0-9]+:[0-9]+$#', $form_state->getValue('aspect_ratio'))) {
      $form_state->setErrorByName('aspect_ratio', $this->t('Invalid aspect ratio format. Should be defined in H:W form.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['anchor'] = $form_state->getValue('anchor');
    $this->configuration['aspect_ratio'] = trim($form_state->getValue('aspect_ratio'));
  }

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    [$w, $h] = explode(':', $this->configuration['aspect_ratio']);
    [$x, $y] = explode('-', $this->configuration['anchor']);
    if (($image->getWidth() * $h / $w) > $image->getHeight()) {
      $width = $image->getHeight() * $w / $h;
      $height = $image->getHeight();
      $x = image_filter_keyword($x, $image->getWidth(), $width);
      $y = 0;

    }
    else {
      $width = $image->getWidth();
      $height = $image->getWidth() * $h / $w;
      $x = 0;
      $y = image_filter_keyword($y, $image->getHeight(), $height);
    }
    if (!$image->apply('crop_ratio',
      [
        'x' => $x,
        'y' => $y,
        'width' => $width,
        'height' => $height,
      ]
    )) {
      $this->logger->error('Image crop failed using the %toolkit toolkit on %path (%mimetype, %aspect_ratio)',
        [
          '%toolkit' => $image->getToolkitId(),
          '%path' => $image->getSource(),
          '%mimetype' => $image->getMimeType(),
          '%aspect_ratio' => $this->configuration['aspect_ratio'],
        ]
      );
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions, $uri)
  {
    [$w, $h] = explode(':', $this->configuration['aspect_ratio']);
    if (($dimensions['width'] * $h / $w) > $dimensions['height']) {
      $dimensions['width'] = $dimensions['height'] * $w / $h;
    }
    else {
      $dimensions['height'] = $dimensions['width']  * $h / $w;
    }
  }
}
