### Summary
This module provides an image style effect that crops image to predefined ratio without resizing it. So, instead of setting width and height parameters in pixels as you would for core crop effect, you need to set up an aspect ratio, and the protruding dimension will be cropped, keeping another dimension intact. This way you can get images of different sizes with same aspect ratio (which can be useful, eg, for some zoom gallery).

There are more sophisticated modules such as Image Crop Widget or Focal Point which provide widgets with control over every single image.

### Requirements
GD image processing lbrary (which is default) and core image module.

### Installation
Standard.

### Configuration
Standard image style effect - add to the image style and set up aspect ratio and cropping anchor.

